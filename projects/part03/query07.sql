.read data.sql

select
(
  -- Friend of Cassandra
  select COUNT(*) from Highschooler as cas, Highschooler as hf
    -- friend of Cassandra or Cassandra's friend
    inner join Friend as f
      on (f.ID1=cas.ID and f.ID2=hf.ID) or (f.ID1=hf.ID and f.ID2=cas.ID)
  where cas.ID=1709
)
  +
(
  -- Friend of a friend of Cassandra
  select COUNT(*) from Highschooler as cas, Highschooler as hf, Highschooler as hff
    -- friend of Cassandra or Cassandra's friend
    inner join Friend as f
      on (f.ID1=cas.ID and f.ID2=hf.ID) or (f.ID1=hf.ID and f.ID2=cas.ID)
    -- Cassandra's friend's friend or friend of Cassandra's friend
    inner join Friend as ff
      on (ff.ID1=hf.ID and ff.ID2=hff.ID) or (ff.ID1=hff.ID and ff.ID2=hf.ID)
  -- root on Cassandra's id, don't loopback to same id
  where cas.ID=1709 and cas.ID <> hff.ID
);
