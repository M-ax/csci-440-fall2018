.read data.sql

insert into Friend select distinct h1.ID, h3.ID
  from Highschooler as h1, Highschooler as h2, Highschooler as h3
  where ((h1.ID, h2.ID) in Friend or (h2.ID, h1.ID) in Friend)
    and ((h2.ID, h3.ID) in Friend or (h3.ID, h2.ID) in Friend)
    and ((h1.ID, h3.ID) not in Friend and (h3.ID, h1.ID) not in Friend)
    and h1.ID <> h3.ID;
