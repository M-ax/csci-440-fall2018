.read data.sql

select distinct h1.name, h1.grade from Highschooler as h1
  inner join Friend as f on f.ID1 = h1.ID
  inner join Highschooler as h2 on f.ID2=h2.ID
  where h1.grade <> h2.grade;
