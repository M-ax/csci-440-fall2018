.read data.sql

select h1.name, h1.grade, h2.name, h2.grade, h3.name, h3.grade
  from Highschooler as h1, Highschooler as h2, Highschooler as h3
  where
    (h1.ID, h2.ID) in Likes
    and (h2.ID, h3.ID) in Likes
    and h1.ID <> h3.ID;
