.read data.sql

select distinct h1.name, h1.grade from Highschooler as h1
  where not exists (
    select * from Highschooler as h2
      where ((h1.ID, h2.ID) in Friend or (h2.ID, h1.ID) in Friend)
        and h2.grade <> h1.grade
  )
  order by grade asc, name asc;
