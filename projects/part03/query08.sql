.read data.sql

select h.name, h.grade, COUNT(*) as friendCount from Highschooler as h
  inner join Friend as f on f.ID1=h.ID
  group by f.ID1
  order by friendCount desc
  limit 1;
