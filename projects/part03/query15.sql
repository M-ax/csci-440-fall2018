.read data.sql

select h1.name, h1.grade, h2.name, h2.grade, h3.name, h3.grade
  from Highschooler as h1, Highschooler as h2, Highschooler as h3
  inner join Likes as l
    on (l.ID1=h1.ID and l.ID2=h2.ID) or (l.ID1=h2.ID and l.ID2=h1.ID)
  -- h1 not friends with h2 and h2 not friends with h1
  where (h1.ID, h2.ID) not in Friend and (h2.ID, h1.ID) not in Friend
    -- h3 can bridge h1 -> h3 or h3 -> h1
    and ((h1.ID, h3.ID) in Friend or (h3.ID, h1.ID) in Friend)
    -- AND they can bridge h3 -> h2 or h2 -> h3
    and ((h3.ID, h2.ID) in Friend or (h2.ID, h3.ID) in Friend)
    -- Avoid duplicate triplets by taking one side of the relation
    and h1.name < h2.name;
