.read data.sql

select h.name, h.grade from Highschooler as h
  where not exists (select * from Likes as l where l.ID1 = h.ID or l.ID2 = h.ID)
  order by grade asc, name asc;
