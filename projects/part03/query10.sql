.read data.sql

select h1.name, h1.grade, h2.name, h2.grade from Highschooler as h1, Highschooler as h2
  inner join Likes as l on l.ID1 = h1.ID and l.ID2 = h2.ID
  where h1.grade - h2.grade >= 2;
