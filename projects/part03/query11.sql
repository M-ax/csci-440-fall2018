.read data.sql

select h1.name, h1.grade, h2.name, h2.grade from Highschooler as h1, Highschooler as h2
  -- h1 likes h2
  inner join Likes as l1 on l1.ID1 = h1.ID and l1.ID2 = h2.ID
  -- h2 likes h1
  inner join Likes as l2 on l2.ID1 = h2.ID and l2.ID2 = h1.ID
  where h1.name < h2.name;
