.read data.sql

delete from Likes as l where exists (
  select h1.ID, h2.ID
  from Highschooler as h1, Highschooler as h2
    inner join Friend
      on (Friend.ID1=h1.ID and Friend.ID2=h2.ID
      or Friend.ID1=h2.ID and Friend.ID2=h1.ID)
  where l.ID1=h1.ID and l.ID2=h2.ID and (h2.ID, h1.ID) not in Likes
);
