# Team Members

* Max Weimer

# Getting started with SQLite3
This tutorial would cover the basics of how to get SQLite3 running on your platform of choice, from downloading the correct binaries, to creating a database and or importing an existing database or tables (ex: csv).

This would be of value because many people, especially those in a database oriented class can sometimes be less on the power-user side of things, and a guiding hand in the right direction might be all that's needed to get someone over the setup hurdle and start being productive in learning how to manage a database.

# Breaking down a schema to Normal Forms
Cover step-by-step the process in which someone would go about converting an existing schema to 1, 2 and 3NF.  First giving an overview of the steps and concepts, then walking through manipulating an example schema.

This would be valuable because normal forms are very useful to create a database with meaningful relationships and predictable, defined behaviors, but are very specific and difficult to conform to on-the-fly without understanding the inner workings of how and why they're used.

# Working with multiple tables: JOIN operations
Provide the reader with an understanding of how, when, and why to use the join operator, and other similar operators to pull related data from different tables.

Join operations are incredibly useful when working with real-world datasets, as very rarely is all of the data that you need in one table.  More often data is spread across many tables with complex 1:N and M:N relationships between all of them, understanding how to practically pull all of this data together is not just convenient, it's necessary to work with a complex schema.
