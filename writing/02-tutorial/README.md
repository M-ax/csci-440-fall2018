# Title

Here you will write your tutorial.  Since we are using markdown, you can easily
include code such as:

    > select * from Table

Or local images (such as this cool image of champ)

![MSU Champ](./images/champ.jpg "Champ")

For more on using markdown see [John Gruber's
tutorial](https://daringfireball.net/projects/markdown/)

